This project created for USI Insurance Company

## Установка

Установить Node.js
Выполнить "npm install"
Выполнить "npm run usi"

## Available Commands

## npm run start

Запускает скрипт

## npm run dev

Запускает скрипт(режим авто-перезапуска при изменении файлов)

## npm run usi

Запускает скрипт, перезапускает его каждые 6 часов, показывает панель мониторинга в консоли

## npm run restart

Перезапускает скрипт

## Config Values at ./config/config.js:

### TELEGRAM_TOKEN -> String = Токен Telegram бота

### VIBER_TOKEN -> String = Токен Viber бота

### TELEGRAM_CHAT_ID -> String = Токен Telegram бота

### HTTPS_PORT -> String = Порт для поднятия сервера обработки запросов

### MONGODB_URL -> String = URL MongoDB для хранения данных

### MAX_MESSAGE_LENGTH -> String = Максимальная длина сообщения

### DATA_SAVE_TYPE -> "MongoDB" || "File" = Тип сохранения бекапов данных, MongoDB или локальный файл

### MONGODB_OPTIONS -> String = Настройки для подключения к БД

### USER_MESSAGES_FILE -> String = Путь к файлу, для сохранения данных.
