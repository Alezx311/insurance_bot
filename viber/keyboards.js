// ! Текста кнопок
const Titles = {
  Save: '💾 Зберегти.',
  Cancel: '❗ Cancel',
  MainMenu: '📇 Головне меню.',
  NotifyMenu: `❗ Повідомити про страхову подію.`,
  BuyMenu: `📗 Придбати страховий поліс.`,
  CheckMenu: `❓ Дізнатись стан справи.`,
  Car: '🏎️ ОСЦПВ, або КАСКО.',
  RoadAccident: '🏎️❗ ДТП.',
  Medical: '💯 Туристичне та медичне страхування.',
  Another: '💼 Інші види страхування (вантажі, нерухомість та інше).',
  Covid: '🦠 Захворювання на COVID-19.',
  FinanceRiskInTravel: '💲 Фінансові ризики подорожуючих за кордон.',
  MedicalCompensationInTravel: '⚕️ Медичні витрати за кордоном.',
  IssuesInTravel: '📞 Нещасні випадки за кордоном.',
  IssuesInUkraine: '📞 Нещасні випадки подорожуючих по Україні.'
}
//! Конструктор клавиатур
function createKeyboard(buttons) {
  // Создаём обьект клавиатуры с универсальными параметрами
  const kbObj = {
    Type: 'keyboard',
    Buttons: buttons.map(button => {
      // Для каждой кнопки Viber требует отдельный обьект
      const buttonObj = {
        // Ширина
        Columns: 6,
        // Высота
        Rows: 1,
        // DefaultHeight: true,
        // Текст, тег <i> для курсива
        Text: `<i>${button.text}</i>`,
        TextSize: 'medium',
        // TextShouldFit: true,
        TextHAlign: 'center',
        TextVAlign: 'middle',
        // После нажатия кнопки будет отправлен обычный текст
        ActionType: 'reply',
        // Текст для оптравки
        ActionBody: button.callback_data,
        // Цвет фона
        BgColor: '#F4D03F'
      }
      return buttonObj
    })
  }
  // И возвращаем его
  return kbObj
}
// ! Готовые меню
const Menus = {
  Cancel: createKeyboard([{ text: Titles.Cancel, callback_data: 'route:/MainMenu' }]),
  ToMainMenu: createKeyboard([{ text: Titles.MainMenu, callback_data: 'route:/MainMenu' }]),
  MainMenu: createKeyboard([
    { text: Titles.NotifyMenu, callback_data: 'route:/NotifyMenu' },
    { text: Titles.BuyMenu, callback_data: 'route:/BuyMenu' },
    { text: Titles.CheckMenu, callback_data: 'route:/CheckMenu' }
  ]),
  NotifyMenu: createKeyboard([
    { text: Titles.Car, callback_data: 'route:/NotifyMenu/Car' },
    { text: Titles.Medical, callback_data: 'route:/NotifyMenu/Human' },
    { text: Titles.Another, callback_data: 'route:/NotifyMenu/Another' },
    { text: Titles.MainMenu, callback_data: 'route:/MainMenu' }
  ]),
  NotifyMenuHuman: createKeyboard([
    { text: Titles.Covid, callback_data: 'route:/NotifyMenu/Human/Covid' },
    { text: Titles.FinanceRiskInTravel, callback_data: 'route:/NotifyMenu/Human/FinanceRiskInTravel' },
    { text: Titles.MedicalCompensationInTravel, callback_data: 'route:/NotifyMenu/Human/MedicalCompensationInTravel' },
    { text: Titles.IssuesInTravel, callback_data: 'route:/NotifyMenu/Human/IssuesInTravel' },
    { text: Titles.IssuesInUkraine, callback_data: 'route:/NotifyMenu/Human/IssuesInUkraine' },
    { text: Titles.MainMenu, callback_data: 'route:/MainMenu' }
  ]),
  BuyMenu: createKeyboard([
    { text: Titles.Car, callback_data: 'route:/BuyMenu/Car' },
    { text: Titles.Medical, callback_data: 'route:/BuyMenu/Medical' },
    { text: Titles.Another, callback_data: 'route:/BuyMenu/Another' },
    { text: Titles.MainMenu, callback_data: 'route:/MainMenu' }
  ]),
  CheckMenu: createKeyboard([
    { text: Titles.RoadAccident, callback_data: 'route:/CheckMenu/Car' },
    { text: Titles.Medical, callback_data: 'route:/CheckMenu/Medical' },
    { text: Titles.Another, callback_data: 'route:/CheckMenu/Another' },
    { text: Titles.MainMenu, callback_data: 'route:/MainMenu' }
  ]),
  CheckMenuMedical: createKeyboard([
    { text: Titles.Covid, callback_data: 'route:/CheckMenu/Medical/Covid' },
    { text: Titles.FinanceRiskInTravel, callback_data: 'route:/CheckMenu/Medical/FinanceRiskInTravel' },
    { text: Titles.MedicalCompensationInTravel, callback_data: 'route:/CheckMenu/Medical/MedicalCompensationInTravel' },
    { text: Titles.IssuesInTravel, callback_data: 'route:/CheckMenu/Medical/IssuesInTravel' },
    { text: Titles.IssuesInUkraine, callback_data: 'route:/CheckMenu/Medical/IssuesInUkraine' },
    { text: Titles.MainMenu, callback_data: 'route:/MainMenu' }
  ]),
  Settings: createKeyboard([{ text: Titles.MainMenu, callback_data: 'route:/MainMenu' }]),
  Help: createKeyboard([{ text: Titles.MainMenu, callback_data: 'route:/MainMenu' }])
}

module.exports = Menus
