const TextMessage = require('viber-bot').Message.Text
const helpers = require('../config/helpers')
const keyboards = require('./keyboards')
const messages = require('./messages')

//! Функция для отправки ответа в виде текста и/или меню. Если меню не указано, отправляется кнопка со ссылкой на главное меню
function say(res, text, kb = keyboards.ToMainMenu, userTracking = null) {
  // Если нет обьекта ответа, или текста сообщения, отправить не получится
  if (!res || !text) {
    return helpers.cuteError('Unknown values at say()', res, text)
  }
  // Отправляем сообщение с указанным текстом, клавиатурой и данными пользователя
  return res.send(new TextMessage(text, kb), userTracking)
}
//! Функция для отправки следующего в опроснике вопроса
function ask(res, userObj) {
  helpers.cuteLog('ask() init', userObj)
  // Если каких то данных не хватает, прерываем процесс
  if (!res || !userObj) {
    helpers.cuteError('Unknown values at save()', res, userObj)
  }
  // Извлекаем значения которые помогут определить текущее состояние
  const { now, stages } = userObj
  const stage = stages[now]
  // Если индекс этапа больше чем вопросов, значит мы закончили опроc
  if (!stage) {
    helpers.cuteLog('ask() save results')
    // Cохраняем данные
    return save(res, userObj)
  }
  // Получаем следующий вопрос
  const question = messages.questions[stage]
  helpers.cuteLog('ask() Next Question:', question)
  // и отправляем его, c данными пользователя
  return say(res, question, keyboards.Cancel, userObj)
}
//! Функция сохранения данных после завершения опроса
async function save(res, userObj) {
  // Если каких то данных не хватает, прерываем процесс
  if (!res || !userObj) {
    helpers.cuteError('Unknown values at save()', res, userObj)
  }
  try {
    helpers.cuteLog('Save() new Request!', userObj)
    // Извлекаем данные для формирования сообщения
    const userMessage = await helpers.viber.createUserMessage(userObj)
    // Сохраняем в БД\Файл
    await helpers.saveUserMessage(userMessage)
    // Отправляем сообщение в рабочий чат Telegram (как иронично...)
    await helpers.telegram.sendNotification(userMessage)
    // Оповещаем что данные сохранены
    return say(res, messages.SaveSuccess, keyboards.MainMenu)
  } catch (err) {
    helpers.cuteError('Error on saving Viber User Message', err)
    return say(res, messages.SaveError, keyboards.MainMenu)
  }
}
//! Функция для отправки опросника
function quiz(res, routerPath, last = 'phone') {
  // Если нет обьекта ответа, или выбранного пути, просим повторить
  if (!res || !routerPath) {
    helpers.cuteError('Unknown values in quiz()', res, routerPath)
    return say(res, messages.СhooseMenu, keyboards.MainMenu)
  }
  // Уникальный id пользователя, для идентификации
  const { id } = res.userProfile
  // Массив с уникальными темами вопросов
  const stages = [...new Set(['name', 'phone', last])]
  // Обьект для занесения данных пользователя и опросника
  const user = { name: null, phone: null, policy: null, accident: null, now: 0, stages, routerPath, id }
  // Отправляем опросник, функция ask(), отправляет вопросы по очереди
  return ask(res, user)
}
//! Функция для отправки меню
function menu(message, res) {
  helpers.cuteLog('Route handler was activated for message:', message)
  // Для того чтобы не изобретать велосипед, просто сравниваем значение кнопки
  const route = message?.text
  //? ########## Main Menu ##########
  if (route === 'route:/MainMenu') return say(res, messages.MainMenu, keyboards.MainMenu)
  if (route === 'route:/NotifyMenu') return say(res, messages.NotifyMenu, keyboards.NotifyMenu)
  if (route === 'route:/BuyMenu') return say(res, messages.BuyMenu, keyboards.BuyMenu)
  if (route === 'route:/CheckMenu') return say(res, messages.CheckMenu, keyboards.CheckMenu)
  //? ########## Notify Menu ###########
  if (route === 'route:/NotifyMenu/Car') return quiz(res, route)
  if (route === 'route:/NotifyMenu/Human') return say(res, messages.NotifyMenuHuman, keyboards.NotifyMenuHuman)
  if (route === 'route:/NotifyMenu/Human/Covid') return quiz(res, route, 'policy')
  if (route === 'route:/NotifyMenu/Human/FinanceRiskInTravel') return quiz(res, route, 'policy')
  if (route === 'route:/NotifyMenu/Human/MedicalCompensationInTravel') return quiz(res, route, 'policy')
  if (route === 'route:/NotifyMenu/Human/IssuesInTravel') return quiz(res, route, 'policy')
  if (route === 'route:/NotifyMenu/Human/IssuesInUkraine') return quiz(res, route, 'policy')
  if (route === 'route:/NotifyMenu/Another') return quiz(res, route, 'policy')
  //? ########## Buy Menu ###########
  if (route === 'route:/BuyMenu/Car') return quiz(res, route)
  if (route === 'route:/BuyMenu/Medical') return quiz(res, route)
  if (route === 'route:/BuyMenu/Another') return quiz(res, route)
  //? ########## Check Menu ###########
  if (route === 'route:/CheckMenu/Car') return quiz(res, route, 'accident')
  if (route === 'route:/CheckMenu/Medical') return say(res, messages.CheckMenuMedical, keyboards.CheckMenuMedical)
  if (route === 'route:/CheckMenu/Medical/Covid') return quiz(res, route, 'policy')
  if (route === 'route:/CheckMenu/Medical/FinanceRiskInTravel') return quiz(res, route, 'policy')
  if (route === 'route:/CheckMenu/Medical/MedicalCompensationInTravel') return quiz(res, route, 'policy')
  if (route === 'route:/CheckMenu/Medical/IssuesInTravel') return quiz(res, route, 'policy')
  if (route === 'route:/CheckMenu/Medical/IssuesInUkraine') return quiz(res, route, 'policy')
  if (route === 'route:/CheckMenu/Another') return quiz(res, route, 'policy')
  //? ########## If Route Not Finded ##########
  helpers.cuteLog(`Cant find router!`, message)
  // Если подходящего пути не найдено, отправляем предложение выбрать сначала пункт меню
  return choose(message, res)
}
//! Обработчик для ответов пользователя
function answer(message, res) {
  helpers.cuteLog('Answer() init!', message)
  // Если данных пользователя не найдено, отправляем предложение выбрать сначала пункт меню
  if (!message.trackingData.id) {
    helpers.cuteLog('Answer() unknown tracking data', message)
    return choose(message, res)
  }
  // Если данные найдены, смотрим, какие данные мы спрашивали
  const { trackingData } = message
  const { now, stages } = trackingData
  const stage = stages[now]
  //  Сохраняем их
  trackingData[stage] = message.text
  trackingData.now++
  helpers.cuteLog('Answer() message saved', trackingData)
  // И отправляем следующий вопрос
  return ask(res, trackingData)
}
//! Обработчик для быстрых команд
function command(message, res) {
  // Команды /start или /main выводят главное меню
  if (message.text === '/start' || message.text === '/main') {
    return say(res, messages.MainMenu, keyboards.MainMenu)
  }
  // Если подходящей команды не найдено, отправляем соответствующее сообщение
  helpers.cuteError('Unknown command:', message)
  return say(res, messages.UnknownCommand, keyboards.MainMenu)
}
//! Для ошибок, отправляет предложение выбрать какой либо пункт меню, и потом уже прислать данные
function choose(message, res) {
  helpers.cuteLog('Message was received, but no info was finded!', message)
  return say(res, messages.СhooseMenu, keyboards.MainMenu)
}

module.exports = { menu, answer, command }
