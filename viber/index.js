const Viber = require('viber-bot')
const config = require('../config/config')
const helpers = require('../config/helpers')
const routes = require('./routes')
//! Инициализация бота
async function init(server, url) {
  // Winston логгер
  const logger = helpers.createLogger()
  // Проверяем наличие сервера и адреса для отправки запросов
  if (!server || !url) {
    throw new Error(`Unknown server or url for init Telegram Bot: ${url}`)
  }
  // Проверяем чтобы в конфиге был указан токен для бота
  if (!config.VIBER_TOKEN) {
    throw new Error('Unknown VIBER_TOKEN at config!')
  }
  // Создаём бота
  const bot = new Viber.Bot({
    authToken: config.VIBER_TOKEN,
    // logger,
    name: 'USI Bot',
    avatar: 'https://usi.net.ua/images/car.png'
    // avatar: 'https://gravatar.com/avatar/f642151da6102d8644b406cf93791e7e?s=400&d=robohash&r=x'
  })
  // Ставим обработчик запросов на поднятом локальном сервере
  server.use('/viber', bot.middleware())
  // И указываем адрес для прослушки запросов ботом
  bot.setWebhook(`${url}/viber`)
  // Так как у Viber нет отдельного события для кнопок, используем регулярки, для определения типа события
  bot.on(Viber.Events.MESSAGE_RECEIVED, (message, response) => {
    // Если была нажата кнопка, обрабатываем её
    if (message.text.match(/^route:\//i)) return routes.menu(message, response)
    // Если была отправлена команда, обрабатываем её
    if (message.text.match(/^\//i)) return routes.command(message, response)
    // Иначе, проверяем сообщение на результат опроса
    return routes.answer(message, response)
  })
}

module.exports = { init }
