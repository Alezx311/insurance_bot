// ! Примеры для опросника
const examples = {
  Name: `Іванов Іван Іванович`,
  Phone: `+380501234567`,
  Policy: `123456789`,
  Accident: `123456789`
}
// ! Готовые сообщения для вывода в боте
const messages = {
  Welcome: `Вас вітає страхова компанія USI.`,
  Сhoose: `Оберіть нижче, що вас цікавить.`,
  СhooseMenu: 'Будь ласка, виберіть пункт меню, і напишіть ваші дані після запиту',
  UnknownCommand: 'Unknown Command',
  questions: {
    name: `Напишіть ваше ПІБ.\n\nНаприклад: ${examples.Name}`,
    phone: `Напишіть ваш контактний номер телефону.\n\nНаприклад: ${examples.Phone}`,
    policy: `Напишiть номер вашого Договору страхування.\n\nНаприклад: ${examples.Policy}`,
    accident: `Напишіть ваш номер справи.\n\nНаприклад: ${examples.Accident}`
  },
  AskName: `Напишіть ваше ПІБ.\n\nНаприклад: ${examples.Name}`,
  AskPhone: `Напишіть ваш контактний номер телефону.\n\nНаприклад: ${examples.Phone}`,
  AskPolicy: `Напишіть ваш ваш номер Договору страхування.\n\nНаприклад: ${examples.Policy}`,
  AskAccident: `Напишіть ваш ваш номер справи.\n\nНаприклад: ${examples.Accident}`,
  AskUserDataSimple: `Напишіть ваше ПІБ, та контактний номер телефону.\nНаприклад: \n${examples.Name}\n${examples.Phone}`,
  AskUserDataPolicy: `Напишіть ваше ПІБ, контактний номер телефону, та ваш номер Договору страхування.\nНаприклад: \n${examples.Name}\n${examples.Phone}\n${examples.Policy}`,
  AskUserDataAccident: `Напишіть ваше ПІБ, контактний номер телефону, та ваш номер справи.\nНаприклад: \n${examples.Name}\n${examples.Phone}\n${examples.Accident}`,
  MainMenu: `Головне меню`,
  NotifyMenu: `Головне меню -> ❗ Повідомити`,
  NotifyMenuHuman: 'Головне меню -> ❗ Повідомити -> 💯 Туристичне та медичне страхування.',
  NotifyMenuCar: 'Головне меню -> ❗ Повідомити -> 🏎️ ОСЦПВ, або КАСКО.',
  BuyMenu: `Головне меню -> 📗 Придбати`,
  CheckMenu: `Головне меню -> ❓ Дізнатись`,
  CheckMenuMedical: `Головне меню -> ❓ Дізнатись -> 💯 Туристичне та медичне страхування.`,
  Help: `Про цього бота`,
  Settings: `Налаштування`,
  SaveError: `Ой, що щось пішло не так, спробуйте ще раз ...`,
  SaveSuccess: `Ваше повідомлення надіслано нашій команді підтримки, ми з вами зв'яжемось у найближчий час.\nМожливо, ми ще чимось можемо вам допомогти?`,
  SaveProcess: `Ваше повідомлення надсилається, почекайте, будь ласка ...`
}

module.exports = messages
