const mongoose = require('mongoose')

// ! Схема для сохранения всех данных
const userEventSchema = new mongoose.Schema({
  from: { type: Object, required: true },
  update: { type: Object, required: true },
  text: { type: String, required: true },
  menu: { type: String, required: true }
})
const UserEvent = mongoose.model('UserEvent', userEventSchema)

// ! Схема для сохранения только лога
const userMessageSchema = new mongoose.Schema({
  text: { type: String, required: true }
})
const UserMessage = mongoose.model('UserMessage', userMessageSchema)

module.exports = { UserEvent, UserMessage }
