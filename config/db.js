const mongoose = require('mongoose')
const config = require('./config')
mongoose.Promise = global.Promise

// ! Получить соединение
function getConnection() {
  if (!config.MONGODB_URL || !config.MONGODB_OPTIONS) {
    throw new Error('Unknown MONGODB_URL or MONGODB_OPTIONS at config!')
  }

  mongoose.connect(config.MONGODB_URL, config.MONGODB_OPTIONS)

  mongoose.connection.on('connected', () => console.log('mongoose connected successfully!'))
  mongoose.connection.on('disconnected', data => console.warn(`mongoose disconnected!\n${data}`, data))
  mongoose.connection.on('error', err => {
    console.error(`mongoose error!`, err)
    throw err
  })

  return mongoose.connection
}
// ! Разорвать соединение
function closeConnection() {
  return mongoose.connection.close()
}
// ! Получить лог с сообщением о статусе соединения
function checkConnection() {
  const states = [`Not Connected`, `Connected`]
  const state = mongoose.connection.readyState
  const stateMessage = states?.[state] ?? 'Unknown'
  console.log(`mongoose connection state: ${state} - ${stateMessage}`)

  return state === 1
}

module.exports = {
  getConnection,
  closeConnection,
  checkConnection
}
