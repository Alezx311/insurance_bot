const config = require('./config')
const validate = require('./validate')
const db = require('./db')
const Models = require('./models')
const gradient = require('gradient-string')
const fs = require('fs')
const winston = require('winston')
const axios = require('axios').default
const needle = require('needle')
const expressWinston = require('express-winston')

let telegramBot = null

//! Часто используемые мелкие функции? для удобства

//! Подготовка приложения, проверки и тд
function prepareApp() {
  // Проверяем наличие токенов для запуска ботов
  if (!config.TELEGRAM_TOKEN || !config.VIBER_TOKEN) {
    throw new Error('Unknown bot tokens at config file!')
  }
  // Проверяем наличие id чата для отсылки сообщений от пользователей
  if (!config.TELEGRAM_CHAT_ID) {
    throw new Error('Unknown TELEGRAM_CHAT_ID at config file!')
  }
  // Проверяем наличие указанного пути к файлу для записи сообщений от пользователей
  if (!config.USER_MESSAGES_FILE) {
    throw new Error('Unknown USER_MESSAGES_FILE at config file!')
  }
  // Проверяем наличие файла для записи сообщений от пользователей, и разрешения за запись
  fs.access(config.USER_MESSAGES_FILE, fs.constants.W_OK, async err => {
    if (err) {
      cuteError(`File at ${config.USER_MESSAGES_FILE}: dont have permissions to write!`, err)
      // Если что то не так, пытаемся создать новый
      await createFile(config.USER_MESSAGES_FILE, `File created at ${createTimeString()}`)
    }
    // Записываем в лог что приложение было запущено, функция записи добавит метку времени
    return await appendToFile(config.USER_MESSAGES_FILE, 'App Started!')
  })
}
//! Логгер для Express сервера
function expressLogger() {
  return expressWinston.logger({
    // Записываем в консоль и отдельный файл
    transports: [new winston.transports.File({ filename: 'serverLogs.log' })],
    format: winston.format.prettyPrint(),
    meta: true,
    msg: 'HTTP {{req.method}} {{req.url}}',
    expressFormat: true,
    colorize: false
  })
}
//! Создаёт обычный логгер, используется для обоих ботов
function createLogger() {
  return winston.createLogger({
    // Уровень можно будет сменить до 'info' если логов будет слишком много
    level: 'debug',
    format: winston.format.prettyPrint(),
    defaultMeta: { service: process.cwd() },
    transports: [
      new winston.transports.Console(),
      new winston.transports.File({ filename: 'appLogs.log' }),
      // Ошибки в отдельный файл
      new winston.transports.File({ filename: 'appErrors.log', level: 'error' })
    ],
    // Исключения и выбросы тоже в отдельные файлы
    exceptionHandlers: [new winston.transports.File({ filename: 'exceptions.log' })],
    rejectionHandlers: [new winston.transports.File({ filename: 'rejections.log' })]
  })
}
//! Для создание файла по указанному пути
function createFile(filepath, content = '') {
  if (!filepath) {
    throw new Error('Unknown filepath on createFile()!')
  }
  // Создаём файл с переданными данными
  fs.writeFile(filepath, content, err => {
    if (err) {
      cuteErr('Error on creating file', err)
      throw err
    } else {
      return cuteLog(`File ${filepath} created successfully!`)
    }
  })
}
//! Добавление данных в имеющийся файл
function appendToFile(filepath, content) {
  if (!filepath || !content) {
    throw new Error('Unknown filepath or content at appendContentToFile()!')
  }
  // Форматируем сообщение, добавляя метку времени, отступы и тд
  const contentMessage = `\n${'#'.repeat(5)} New Message ${'#'.repeat(5)}\n${content}\n`
  // И добавляем в конец файла
  fs.appendFile(filepath, contentMessage, err => {
    if (err) {
      throw new Error('Error on append content to file!', err)
    } else {
      return true
    }
  })
}
//! Создаёт красивую строку с временем
function createTimeString() {
  return `Время: ${new Date(Date.now()).toUTCString()}`
}
//! Конвертация данных в строку
function toString(data) {
  // Если на входе обьект, форматируем через табуляцию
  if (validate.object(data)) {
    return String(JSON.stringify(data, null, '\t'))
  }
  // Если массив, просто проверяем каждый елемент
  if (validate.array(data)) {
    return String(data.map(toString))
  }
  // И возвращаем строку
  return String(data)
}
//! Проверяет длину текста и при необходимости обрезает до указанного значения, по умолчанию = 2000 символов
function toMaxLength(str, max = config.MAX_MESSAGE_LENGTH) {
  if (!str) {
    throw new Error('Unknown string to cut on toMaxLength()!')
  }
  // Если был передан другой тип данных, конвертируем в строку.
  if (typeof str !== 'string') {
    str = toString(str)
  }
  // Если строка длиннее, обрезаем
  if (str.length < max) {
    str = str.substring(0, max)
  }
  return String(str)
}
//! Сохраняет сообщение о действии пользователя, в БД и файл
async function saveUserMessage(message) {
  // Проверяем какой способ хранения указан, и при необходимости сохраняем в БД
  if (config.DATA_SAVE_TYPE === 'MongoDB') {
    await saveUserEventMessageToMongo(message)
  }
  // И в текстовый файл
  return await appendToFile(config.USER_MESSAGES_FILE, message)
}
//! Сохраняет сообщение в MongoDB
async function saveUserEventMessageToMongo(message) {
  if (!ctx) {
    throw new Error('Unknown context on saveUserEventMessageToMongo()!')
  }
  // Создаём модель MongoDB для сообщений
  const userMessage = new Models.UserMessage(message)
  // Открываем соединение c MongoDB
  await db.getConnection()
  // Сохраняем сообщение
  await userMessage.save()
  cuteLog('User Message saved to MongoDB', userMessage)
  // Закрываем коннект к MongoDB
  await db.closeConnection()
  // Возвращаем сообщение, уже с _id
  return userMessage
}

async function sendUserMessageToUsi(userDataObject) {
  try {
    const toValidText = text => text.replace(/[^a-zа-я0-9і]+/gi, ' ').trim()

    const subject = toValidText(userDataObject.name)
    const task = toValidText(userDataObject.route)
    const comment = toValidText(userDataObject.comment)

    const body = {
      action: 'setTask',
      data: {
        date_start: new Date(Date.now()),
        subject,
        task,
        comment
      }
    }
    // const res = 'resssssss'
    const res = await axios({
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      url: 'https://tau.scudcrm.com/api/v3/usi/set/?key=Jc6Q4DaXVaJVAeE7',
      data: body
    })
    cuteLog('sendUserMessageToUsi success')
    console.dir(res)
  } catch (err) {
    console.error('sendUserMessageToUsi error', err)
    throw err
  }
}
//! Конвертирует строку выбранного пункта меню в читаемый вид
function routeTranslate(original) {
  if (!original) return cuteError('Unknown string at routeTranslate()!')
  // Значения для перевода пунктов меню
  const RoutesText = {
    Save: '💾 Зберегти.',
    Cancel: '❗ Cancel',
    MainMenu: '📇 Головне меню.',
    NotifyMenu: `❗ Повідомити про страхову подію.`,
    BuyMenu: `📗 Придбати страховий поліс.`,
    CheckMenu: `❓ Дізнатись стан справи.`,
    Car: '🏎️ ОСЦПВ, або КАСКО.',
    Human: '💯 Туристичне та медичне страхування.',
    Medical: '💯 Туристичне та медичне страхування.',
    Covid: '🦠 Захворювання на COVID-19.',
    FinanceRiskInTravel: '💲 Фінансові ризики подорожуючих за кордон.',
    MedicalCompensationInTravel: '⚕️ Медичні витрати за кордоном.',
    IssuesInTravel: '📞 Нещасні випадки за кордоном.',
    IssuesInUkraine: '📞 Нещасні випадки подорожуючих по Україні.',
    Another: '💼 Інші види страхування (вантажі, нерухомість та інше).',
    RoadAccident: '🏎️❗ ДТП.'
  }
  // Создаём читабельную строку
  const translated = original
    .split('/')
    .map(word => RoutesText?.[word] ?? word)
    .join('\n')
    .replace(/^route:/i, '')
  // Отправляем
  return translated
}
//! Вспомогательные функции для Telegram
const telegram = {
  createUserMessage: async function (ctx) {
    if (!ctx || !ctx.update.message) {
      throw new Error('Unknown context in telegram.createUserMessage()')
    }

    // Извлекаем нужные данные
    const { username, id } = ctx.update.message.from
    const { name, phone, policy, accident } = ctx.session.user
    const route = routeTranslate(ctx.session.routerPath).replace('\n', ' ').trim()
    // Создаём информационное сообщение
    const comment = `ФИО: ${name}
    Телефон: ${phone ?? 'Не указано'}
    Номер полиса: ${policy ?? 'Не указано'}
    Номер дела ДТП: ${accident ?? 'Не указано'}`

    const message = `${createTimeString()}
Источник: Telegram
Профиль: @${username} / <a href="tg://user?id=${id}">Ссылка на пользователя</a>\n
Тема обращения: ${route}\n
${comment}`

    // Посылаем в CRM
    await sendUserMessageToUsi({ name, route, comment })
    // И возвращаем его, при необходимости обрезая
    return toMaxLength(message, 2000)
  },
  // Сохраняет инстанс бота, для отправки оповещений в чат
  saveBotInstance: botInstance => {
    telegramBot = botInstance
    return true
  },
  // Высылает оповещение о действии пользователя в рабочий Telegram чат
  sendNotification: async message => {
    try {
      cuteLog('helpers.telegram.sendNotification new request')
      await bot.telegram.sendMessage(config.TELEGRAM_CHAT_ID, message, { parse_mode: 'HTML' })
      cuteLog('helpers.telegram.sendNotification success')
    } catch (err) {
      cuteError('Erron on sending notification to Telegram!', err)
    }
  }
}
//! Вспомогательные функции для Viber
const viber = {
  // Создаёт читабельное сообщение о действии пользователя
  createUserMessage: async function (userObj) {
    if (!userObj) {
      throw new Error('Unknown user object in viber.createUserMessage()')
    }
    // Извлекаем нужные данные
    const { name, phone, policy, accident, routerPath, id } = userObj
    const route = routeTranslate(routerPath)
    // Создаём удобное для чтения сообщение с данными пользователя
    const comment = `ФИО: ${name}
    Телефон: ${phone ?? 'Не указано'}
    Номер полиса: ${policy ?? 'Не указано'}
    Номер дела ДТП: ${accident ?? 'Не указано'}`
    const message = `${createTimeString()}
Источник: Viber
Viber ID: ${id}\n
Тема обращения: ${route}\n
${comment}`
    // Посылаем в CRM
    await sendUserMessageToUsi({ name, route, comment })
    // И возвращаем его, при необходимости обрезая
    return toMaxLength(message, 2000)
  }
}
//! Выводит в консоль красивый лог
function cuteLog() {
  console.log(gradient.rainbow([...arguments]))
}
//! Выводит в консоль красивый лог ошибки
function cuteError() {
  console.log(gradient.pastel([...arguments]))
}

module.exports = {
  prepareApp,
  expressLogger,
  createLogger,
  createFile,
  appendToFile,
  createTimeString,
  toString,
  toMaxLength,
  saveUserMessage,
  saveUserEventMessageToMongo,
  routeTranslate,
  telegram,
  viber,
  cuteLog,
  cuteError
}
