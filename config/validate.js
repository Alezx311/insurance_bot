const Joi = require('joi')
// ! Валидаторы для более удобного испрользования
const validate = {
  // Проверка на валидный обьект для сохранения в БД
  userEvent: data => {
    const schema = Joi.object({
      from: Joi.object(),
      update: Joi.object(),
      text: Joi.string().min(1),
      menu: Joi.string().min(1)
    })

    return schema.validate(data)
  },
  // Проверка на валидный обьект для создания роута\сообщения\обновления
  createAction: data => {
    const schema = Joi.object({
      ctx: Joi.object(),
      router: Joi.object(),
      params: Joi.object(),
      value: Joi.string(),
      message: Joi.string()
    })

    return schema.validate(data)
  },
  // Проверка на корректный массив с данными для создания клавиатуры
  telegramButtonsArray: data => {
    const schema = Joi.array().has(
      Joi.array().items(
        Joi.object({
          text: Joi.string(),
          callback_data: Joi.string()
        })
      )
    )

    return schema.validate(data)
  },
  // Проверка на корректный массив с данными для создания кнопки клавиатуры
  telegramButton: data => {
    const schema = Joi.array().has(
      Joi.object({
        text: Joi.string(),
        callback_data: Joi.string()
      })
    )

    return schema.validate(data)
  },
  // Проверка на валидное сообщение для отправки
  botMessage: data => {
    const schema = Joi.object({
      message: Joi.string(),
      botMessage: Joi.string().max(2048),
      routerPath: Joi.string(),
      _id: Joi.string(),
      from: Joi.object(),
      update: Joi.object()
    })

    return schema.validate(data)
  },
  // Проверка на строку
  string: data => {
    const schema = Joi.string()

    return schema.validate(data)
  },
  // Проверка на строку и корректную длину, по умолчанию 1024
  stringLength: (data = '', maxLength = 1024) => {
    const schema = Joi.string().min(1).max(maxLength)
    return schema.validate(data)
  },
  // Проверка на массив
  array: data => {
    const schema = Joi.array()

    return schema.validate(data)
  },
  // Проверка на обьект
  object: data => {
    const schema = Joi.object()

    return schema.validate(data)
  },
  // Проверка на число
  number: data => {
    const schema = Joi.number()

    return schema.validate(data)
  },
  // Проверка на булево значение
  boolean: data => {
    const schema = Joi.boolean()
    return schema.validate(data)
  },
  // Проверка на корректное сообщение для лога, только строковый вид и макс длина в 1024 символа
  logMessage: data => {
    const schema = Joi.string().max(1024)
    return schema.validate(data)
  }
}

module.exports = validate
