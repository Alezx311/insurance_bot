const axios = require('axios').default

const examples = [
  {
    name: 'Иванов Иван Иванович 1',
    message: 'Тест 1',
    route: 'тест1'
  },
  {
    name: 'Иванов Иван Иванович 2',
    message: 'Тест 2',
    route: 'тест2'
  },
  {
    name: 'Иванов Иван Иванович 3',
    message: 'Тест 3',
    route: 'тест3'
  }
]

async function sendUserMessageToUsi(userDataObject) {
  try {
    const date = new Date(Date.now())
    const reqUrl = 'https://tau.scudcrm.com/api/v3/usi/set/?key=Jc6Q4DaXVaJVAeE7'
    const reqBody = {
      action: 'setTask',
      data: {
        date_start: `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`,
        time_start: `${date.getHours()}:${date.getMinutes()}`,
        subject: userDataObject.name,
        task: userDataObject.route,
        comment: userDataObject.message
      }
    }

    const res = await axios({
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      url: 'https://tau.scudcrm.com/api/v3/usi/set/?key=Jc6Q4DaXVaJVAeE7',
      data: reqBody
    })

    console.log('sendUserMessageToUsi', res)
  } catch (err) {
    console.error(err)
    throw err
  }
}

async function test() {
  await sendUserMessageToUsi({
    name: 'Иванов Иван Иванович 1',
    message: 'Тест 1',
    route: 'тест1'
  })
}

test()
