module.exports = {
  apps: [
    {
      name: 'usi',
      script: 'bots.js',
      restart_delay: 3000,
      cron_restart: '* */3 * * *',
      max_memory_restart: '200M',
      instances: 1,
      ignore_watch: ['[/\\]./', 'node_modules'],
      env: {
        NODE_ENV: 'DEV',
        TELEGRAM_TOKEN: '1419338240:AAFb3xSRHS4SGpTrFOSU8M56kSvYfWMduHo',
        TELEGRAM_CHAT_ID: '-330937955',
        VIBER_TOKEN: '4c80f768ff400d57-ea9a81f62bb83ed0-ac69f543736da70a',
        HTTPS_PORT: '7777',
        MAX_MESSAGE_LENGTH: '2000',
        DATA_SAVE_TYPE: 'File',
        USER_MESSAGES_FILE: './files/userMessages.txt'
      },
      env_production: {
        NODE_ENV: 'PRODUCTION',
        TELEGRAM_TOKEN: '1459843135:AAEjWfN6Ow8PGpl2aBw5Ywn3Q7A00PshRJI',
        TELEGRAM_CHAT_ID: '-1001171794348',
        VIBER_TOKEN: '4c852ba227000b2f-3fe236501748b031-3584dab89295fa5a'
      }
    }
  ],

  deploy: {
    production: {
      user: 'SSH_USERNAME',
      host: 'SSH_HOSTMACHINE',
      ref: 'origin/master',
      repo: 'GIT_REPOSITORY',
      path: 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
}
