const ngrok = require('ngrok')
const config = require('./config/config')
const helpers = require('./config/helpers')
const express = require('express')
const telegramBot = require('./telegram')
const viberBot = require('./viber')
const app = express()
//! Резервные обработчики исключений, если где то будет необработанная ошибка
process.on('uncaughtException', err => {
  helpers.cuteError('uncaughtException!', err)
})
process.on('unhandledRejection', err => {
  helpers.cuteError('unhandledRejection!', err)
})
//! Логгер для Express сервера
// app.use(helpers.expressLogger())
//! Функция для создания локального сервера для обработки запросов
async function createServer() {
  // htpps туннель для вебхуков от ботов
  const url = await ngrok.connect(config.HTTPS_PORT)
  // Сервер поднимается на http://localhost:${config.HTTPS_PORT}
  app.listen(config.HTTPS_PORT, async err => {
    if (err) {
      helpers.cuteError('Error on start server listening', err)
      throw err
    } else {
      helpers.cuteLog(`Server start successfully!
NODE_ENV=${process.env.NODE_ENV}
HTTPS: ${url} -> http:/localhost:${config.HTTPS_PORT}`)
      // Запуск Telegram бота
      await telegramBot.init(app, url)
      // Запуск Viber бота
      await viberBot.init(app, url)
    }
  })
}

//! Запускаем приложение
createServer()
