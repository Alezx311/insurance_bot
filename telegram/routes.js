const Messages = require('./messages')
const Keyboards = require('./keyboards')

//! Настройка опросника
function setQuestions(ctx, type = 'simple') {
  if (!type || !ctx) {
    throw new Error('Unknown type or context for setQuestions()')
  }

  if (type === 'simple') {
    ctx.session.AskPolicyId = false
    ctx.session.AskCarAccidentId = false
  } else if (type === 'policy') {
    ctx.session.AskPolicyId = true
    ctx.session.AskCarAccidentId = false
  } else if (type === 'car_accident') {
    ctx.session.AskPolicyId = false
    ctx.session.AskCarAccidentId = true
  } else {
    throw new Error(`Unknown type ${type} on setQuestions()!`)
  }
}
// ! Шаблоны входа в опросник
const Childrens = {
  AskUserDataWizard: [
    {
      path: '',
      action: ({ ctx }) => {
        setQuestions(ctx, 'simple')
        ctx.scene.enter('save-data-wizard')
      }
    },
    {
      path: '/message',
      action: () => {
        return true
      }
    }
  ],
  AskUserDataWizardCar: [
    {
      path: '',
      action: ({ ctx }) => {
        setQuestions(ctx, 'car_accident')
        ctx.scene.enter('save-data-wizard')
      }
    },
    {
      path: '/message',
      action: () => {
        return true
      }
    }
  ],
  AskUserDataWizardPolicy: [
    {
      path: '',
      action: ({ ctx }) => {
        setQuestions(ctx, 'policy')
        ctx.scene.enter('save-data-wizard')
      }
    },
    {
      path: '/message',
      action: () => {
        return true
      }
    }
  ]
}
//! Роуты для бота
const routes = [
  // Меню MainMenu
  {
    path: '/MainMenu',
    children: [
      {
        path: '',
        action: ({ ctx }) => {
          ctx.scene.leave()
          ctx.reply(Messages.MainMenu, Keyboards.MainMenu)
        }
      }
    ]
  },
  // Меню NotifyMenu
  {
    path: '/NotifyMenu',
    children: [
      {
        path: '',
        action: ({ ctx }) => {
          ctx.reply(Messages.NotifyMenu, Keyboards.NotifyMenu)
        }
      },
      {
        path: '/Car',
        children: Childrens.AskUserDataWizard
      },
      {
        path: '/Human',
        children: [
          {
            path: '',
            action: ({ ctx }) => {
              ctx.reply(Messages.NotifyMenuHuman, Keyboards.NotifyMenuHuman)
            }
          },
          {
            path: '/Covid',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/FinanceRiskInTravel',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/MedicalCompensationInTravel',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/IssuesInTravel',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/IssuesInUkraine',
            children: Childrens.AskUserDataWizardPolicy
          }
        ]
      },
      {
        path: '/Another',
        children: Childrens.AskUserDataWizardPolicy
      }
    ]
  },
  // Меню BuyMenu
  {
    path: '/BuyMenu',
    children: [
      {
        path: '',
        action: ({ ctx }) => {
          ctx.reply(Messages.BuyMenu, Keyboards.BuyMenu)
        }
      },
      {
        path: '/Car',
        children: Childrens.AskUserDataWizard
      },
      {
        path: '/Medical',
        children: Childrens.AskUserDataWizard
      },
      {
        path: '/Another',
        children: Childrens.AskUserDataWizard
      }
    ]
  },
  // Меню CheckMenu
  {
    path: '/CheckMenu',
    children: [
      {
        path: '',
        action: ({ ctx }) => {
          ctx.reply(Messages.CheckMenu, Keyboards.CheckMenu)
        }
      },
      {
        path: '/Car',
        children: Childrens.AskUserDataWizardCar
      },
      {
        path: '/Medical',
        children: [
          {
            path: '',
            action: ({ ctx }) => {
              ctx.reply(Messages.CheckMenuMedical, Keyboards.CheckMenuMedical)
            }
          },
          {
            path: '/Covid',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/FinanceRiskInTravel',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/MedicalCompensationInTravel',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/IssuesInTravel',
            children: Childrens.AskUserDataWizardPolicy
          },
          {
            path: '/IssuesInUkraine',
            children: Childrens.AskUserDataWizardPolicy
          }
        ]
      },
      {
        path: '/Another',
        children: Childrens.AskUserDataWizardPolicy
      }
    ]
  },
  // Команда '/start'
  {
    path: 'start',
    action: ({ ctx }) => {
      ctx.reply(Messages.Welcome, Keyboards.MainMenu)
    }
  },
  // Команда /help
  {
    path: 'help',
    action: ({ ctx }) => {
      ctx.reply(Messages.Help, Keyboards.MainMenu)
    }
  },
  // Команда /settings
  {
    path: 'settings',
    action: ({ ctx }) => {
      ctx.reply(Messages.Settings, Keyboards.Settings)
    }
  },
  // Команда /reset
  {
    path: 'reset',
    action: ({ ctx }) => {
      // Обнуляем данные пользователя
      ctx.session.user = { name: null, phone: null, сarAccidentId: null, policyId: null }
      // И показываем главное меню
      ctx.reply(Messages.MainMenu, Keyboards.Settings)
    }
  }
]

module.exports = routes
