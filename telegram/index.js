const Telegraf = require('telegraf')
const { Router } = require('telegraf-router')
const scenes = require('./scenes')
const routes = require('./routes')
const config = require('../config/config')
const helpers = require('../config/helpers')
//! Инициализация бота
async function init(server, url) {
  // Winston логгер
  const logger = helpers.createLogger()
  // Проверяем наличие сервера и адреса для отправки запросов
  if (!server || !url) {
    throw new Error(`Unknown server or url for init Telegram Bot: ${url}`)
  }
  // Проверяем чтобы в конфиге был указан токен для бота
  if (!config.TELEGRAM_TOKEN) {
    throw new Error('Empty TELEGRAM_TOKEN value at Config!', config)
  }
  // Создаём бота
  bot = new Telegraf(config.TELEGRAM_TOKEN)
  // Используем session для удобного хранения данных пользователя
  bot.use(Telegraf.session())
  // Используем сцены для последовательного опроса нужных данных
  bot.use(scenes.middleware())
  // Роутер для различных меню
  new Router({
    bot,
    routes,
    logger
  })
  // Ставим обработчик запросов на поднятом локальном сервере
  server.use(bot.webhookCallback('/telegram'))
  // И указываем адрес для прослушки запросов ботом
  bot.telegram.setWebhook(`${url}/telegram`)

  helpers.telegram.saveBotInstance(bot)
}

module.exports = { init }
