// ! Конструктор клавиатуры
function createKeyboard(buttons) {
  // Если кнопки невалидные то ставим главное меню
  if (!buttons) {
    console.trace('Empty buttons for creating menu')
    buttons = Menus.MainMenu
  }
  // Возвращаем обьект с клавиатурой
  return {
    reply_markup: {
      remove_keyboard: true,
      inline_keyboard: buttons
    }
  }
}
// ! Текста кнопок для создания меню
const Titles = {
  Save: '💾 Зберегти.',
  Cancel: '❗ Cancel',
  MainMenu: '📇 Головне меню.',
  NotifyMenu: `❗ Повідомити про страхову подію.`,
  BuyMenu: `📗 Придбати страховий поліс.`,
  CheckMenu: `❓ Дізнатись стан справи.`,
  Car: '🏎️ ОСЦПВ, або КАСКО.',
  RoadAccident: '🏎️❗ ДТП.',
  Medical: '💯 Туристичне та медичне страхування.',
  Another: '💼 Інші види страхування (вантажі, нерухомість та інше).',
  Covid: '🦠 Захворювання на COVID-19.',
  FinanceRiskInTravel: '💲 Фінансові ризики подорожуючих за кордон.',
  MedicalCompensationInTravel: '⚕️ Медичні витрати за кордоном.',
  IssuesInTravel: '📞 Нещасні випадки за кордоном.',
  IssuesInUkraine: '📞 Нещасні випадки подорожуючих по Україні.'
}
//! Часто используемые кнопки
const mainMenuButton = [{ text: Titles.MainMenu, callback_data: 'redirect:/MainMenu' }]
const cancelButton = [{ text: Titles.Cancel, callback_data: 'redirect:/MainMenu' }]
// ! Наборы кнопок, для быстрого создания клавиатур
const Menus = {
  Cancel: [cancelButton],
  MainMenu: [
    [{ text: Titles.NotifyMenu, callback_data: 'redirect:/NotifyMenu' }],
    [{ text: Titles.BuyMenu, callback_data: 'redirect:/BuyMenu' }],
    [{ text: Titles.CheckMenu, callback_data: 'redirect:/CheckMenu' }]
  ],
  NotifyMenu: [
    [{ text: Titles.Car, callback_data: 'redirect:/NotifyMenu/Car' }],
    [{ text: Titles.Medical, callback_data: 'redirect:/NotifyMenu/Human' }],
    [{ text: Titles.Another, callback_data: 'redirect:/NotifyMenu/Another' }],
    mainMenuButton
  ],
  NotifyMenuHuman: [
    [{ text: Titles.Covid, callback_data: 'redirect:/NotifyMenu/Human/Covid' }],
    [{ text: Titles.FinanceRiskInTravel, callback_data: 'redirect:/NotifyMenu/Human/FinanceRiskInTravel' }],
    [
      {
        text: Titles.MedicalCompensationInTravel,
        callback_data: 'redirect:/NotifyMenu/Human/MedicalCompensationInTravel'
      }
    ],
    [{ text: Titles.IssuesInTravel, callback_data: 'redirect:/NotifyMenu/Human/IssuesInTravel' }],
    [{ text: Titles.IssuesInUkraine, callback_data: 'redirect:/NotifyMenu/Human/IssuesInUkraine' }],
    mainMenuButton
  ],
  BuyMenu: [
    [{ text: Titles.Car, callback_data: 'redirect:/BuyMenu/Car' }],
    [{ text: Titles.Medical, callback_data: 'redirect:/BuyMenu/Medical' }],
    [{ text: Titles.Another, callback_data: 'redirect:/BuyMenu/Another' }],
    mainMenuButton
  ],
  CheckMenu: [
    [{ text: Titles.RoadAccident, callback_data: 'redirect:/CheckMenu/Car' }],
    [{ text: Titles.Medical, callback_data: 'redirect:/CheckMenu/Medical' }],
    [{ text: Titles.Another, callback_data: 'redirect:/CheckMenu/Another' }],
    mainMenuButton
  ],
  CheckMenuMedical: [
    [{ text: Titles.Covid, callback_data: 'redirect:/CheckMenu/Medical/Covid' }],
    [{ text: Titles.FinanceRiskInTravel, callback_data: 'redirect:/CheckMenu/Medical/FinanceRiskInTravel' }],
    [
      {
        text: Titles.MedicalCompensationInTravel,
        callback_data: 'redirect:/CheckMenu/Medical/MedicalCompensationInTravel'
      }
    ],
    [{ text: Titles.IssuesInTravel, callback_data: 'redirect:/CheckMenu/Medical/IssuesInTravel' }],
    [{ text: Titles.IssuesInUkraine, callback_data: 'redirect:/CheckMenu/Medical/IssuesInUkraine' }],
    mainMenuButton
  ],
  Settings: [mainMenuButton],
  Help: [mainMenuButton]
}
// ! Готовые клавиатуры
const Keyboards = {
  Cancel: createKeyboard(Menus.Cancel),
  MainMenu: createKeyboard(Menus.MainMenu),
  NotifyMenu: createKeyboard(Menus.NotifyMenu),
  NotifyMenuHuman: createKeyboard(Menus.NotifyMenuHuman),
  BuyMenu: createKeyboard(Menus.BuyMenu),
  CheckMenu: createKeyboard(Menus.CheckMenu),
  CheckMenuMedical: createKeyboard(Menus.CheckMenuMedical),
  Settings: createKeyboard(Menus.Settings),
  Help: createKeyboard(Menus.Help)
}

module.exports = Keyboards
