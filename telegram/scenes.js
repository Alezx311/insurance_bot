const config = require('../config/config')
const { Stage } = require('telegraf')
const WizardScene = require('telegraf/scenes/wizard')
const Messages = require('./messages')
const Keyboards = require('./keyboards')
const helpers = require('../config/helpers')

//! Сцена опроса контактных данных
const saveDataWizard = new WizardScene(
  'save-data-wizard',
  //? Шаг 1: ФИО
  ctx => {
    // Обнуляем данные пользователя
    ctx.session.user = { name: null, phone: null, accident: null, policy: null }
    // Спрашиваем имя
    ctx.reply(Messages.AskName, Keyboards.Cancel)
    // Следующий этап
    ctx.wizard.next()
  },
  //? Шаг 2: Номер телефона
  ctx => {
    // Если пользователь нажал на отмену
    if (ctx.update?.callback_query?.data === 'redirect:/MainMenu') {
      // Выходим из сцены
      ctx.scene.leave()
      ctx.reply(Messages.MainMenu, Keyboards.MainMenu)
    } else {
      // Вносим имя пользователя в контекст
      ctx.session.user.name = ctx.update.message.text
      // Спрашиваем телефон
      ctx.reply(Messages.AskPhone, Keyboards.Cancel)
      // Следующий этап
      ctx.wizard.next()
    }
  },
  //? Шаг 3: Номер полиса\Номер дела по ДТП\Сохранение и выход в меню
  async ctx => {
    // Если пользователь нажал на отмену
    if (ctx.update?.callback_query?.data === 'redirect:/MainMenu') {
      // Выходим из сцены
      ctx.scene.leave()
      ctx.reply(Messages.MainMenu, Keyboards.MainMenu)
    } else {
      // Вносим телефон пользователя в контекст
      ctx.session.user.phone = ctx.update.message.text
      // Если нужно спросить номер ДТП, высылаем запрос, и переходим дальше
      if (ctx.session.AskCarAccidentId) {
        ctx.reply(Messages.AskCarAccidentId, Keyboards.Cancel)
        return ctx.wizard.next()
      }
      // Если нужно спросить номер полиса, высылаем запрос, и переходим дальше
      if (ctx.session.AskPolicyId) {
        ctx.reply(Messages.AskPolicyId, Keyboards.Cancel)
        return ctx.wizard.next()
      }
      // Если нужны только имя и телефон, сохраняем
      await saveUserData(ctx)
      // Выходим из опроса в главное меню
      ctx.scene.leave()
    }
  },
  //? Шаг 4: Сохранение и выход в меню
  async ctx => {
    // Если пользователь нажал на отмену
    if (ctx.update?.callback_query?.data === 'redirect:/MainMenu') {
      // Выходим из сцены
      ctx.scene.leave()
      ctx.reply(Messages.MainMenu, Keyboards.MainMenu)
    } else {
      // TODO Переделать на случай увеличения количества вопросов
      // Сохраняем номер ДТП
      if (ctx.session.AskCarAccidentId) {
        ctx.session.user.accident = ctx.update.message.text
      }
      // Или номер полиса в данные юзера
      if (ctx.session.AskPolicyId) {
        ctx.session.user.policy = ctx.update.message.text
      }
      // Высылаем сообщение, что сохранение началось
      ctx.reply(Messages.SaveProcess)
      // Сохраняем данные юзера
      await saveUserData(ctx)
      // выходим из сцены опроса
      ctx.scene.leave()
    }
  }
)
//! Сохранение данных пользователя в БД или локальный файл
async function saveUserData(ctx) {
  try {
    // Создаём сообщение с данными пользователя и ссылками
    const message = await helpers.telegram.createUserMessage(ctx)
    // сохраняем в локальный файл
    await helpers.saveUserMessage(message)
    // Отправляем оповещение в рабочий чат
    await helpers.telegram.sendNotification(message)
    // Оповещаем пользователя что данные отправлены успешно
    return ctx.reply(Messages.SaveSuccess, Keyboards.MainMenu)
  } catch (err) {
    // Оповещаем пользователя о ошибке
    ctx.reply(Messages.SaveError, Keyboards.MainMenu)
    helpers.cuteError('New Error on saveUserData()!')
    throw err
  }
}
//! Создаём сцену
const stage = new Stage([saveDataWizard])

module.exports = stage
